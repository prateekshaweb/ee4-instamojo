<p><strong><?php _e('Instamojo', 'ee-instamojo'); ?></strong></p>

<p><strong><?php _e('Instamojo Settings', 'ee-instamojo'); ?></strong></p>
<ul>
	<li>
		<strong><?php _e('Button Image URL', 'ee-instamojo'); ?></strong><br />
		<?php _e('Change the image that is used for this payment gateway.', 'ee-instamojo'); ?>
	</li>

	<li>
		<strong><?php _e('Private key', 'ee-instamojo'); ?></strong><br />
		<?php _e('Your agreement private key. Found in the "Integration" tab inside the Instamojo manager.', 'ee-instamojo'); ?>
	</li>

	<li>
		<strong><?php _e('API key', 'ee-instamojo'); ?></strong><br />
		<?php _e('Your API User\'s key. Create a separate API user in the "Users" tab inside the Instamojo manager.', 'ee-instamojo'); ?>
	</li>

	<li>
		<strong><?php _e('Text on Statement', 'ee-instamojo'); ?></strong><br />
		<?php _e('Text that will be placed on cardholder’s bank statement (currently only supported by Clearhaus).', 'ee-instamojo'); ?>
	</li>

	<li>
		<strong><?php _e('Branding ID', 'ee-instamojo'); ?></strong><br />
		<?php _e('The ID of your custom branding template. Leave empty if you have no custom branding options.', 'ee-instamojo'); ?>
	</li>

	<li>
		<?php $docs_payment_methods = 'https://instamojo.com/'; ?>
		<strong><?php _e('Payment Methods', 'ee-instamojo'); ?></strong><br />
		<?php _e('Default: creditcard. Type in the cards you wish to accept (comma separated). See the valid payment types here: ', 'ee-instamojo'); ?>
		<a href="<?php echo $docs_payment_methods; ?>" target="_blank"><strong><?php echo $docs_payment_methods; ?></strong></a>
	</li>

	<li>
		<strong><?php _e('Auto Capture', 'ee-instamojo'); ?></strong><br />
		<?php _e('Automatically capture a payment when authorized.', 'ee-instamojo'); ?>
	</li>
</ul>