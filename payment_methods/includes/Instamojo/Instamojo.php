<?php

namespace Instamojo;

use Instamojo\API\Curl;
use Instamojo\API\CurlException;
use Instamojo\API\ValidationException;
use Instamojo\API\Instamojo;

class Instamojo
{
    /**
     * Contains the Instamojo_Request object.
     *
     **/
    public $request;

    /**
     * __construct function.
     *
     * Instantiates the main class.
     * Creates a client which is passed to the request construct.
     *
     * @auth_string string Authentication string for Instamojo
     */
    public function __construct($auth_string = '')
    {
        $client = new Client($auth_string);
        $this->request = new Request($client);
    }
}
