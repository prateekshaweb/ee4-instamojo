<?php

namespace Instamojo\API;

/**
 * @class       Instamojo_Constants
 *
 * @since       0.1.0
 *
 * @category    Class
 *
 * @author      Patrick Tolvstein, Perfect Solution ApS
 * @docs        http://instamojo.com/api/
 */
class Constants
{
    /**
     * API DEFINITIONS.
     */
    const API_URL = 'https://www.instamojo.com/api/1.1/';
    const API_VERSION = '10';
}
