<?php

namespace Instamojo\API;

/**
 * @class       Instamojo_Client
 *
 * @since       0.1.0
 *
 * @category    Class
 *
 * @author      Patrick Tolvstein, Perfect Solution ApS
 * @docs        http://tech.instamojo.com/api/
 */
class Client
{
    /**
     * Contains cURL instance.
     */
    public $ch;

    /**
     * Contains the authentication string.
     */
    protected $auth_string;

    /**
     * __construct function.
     *
     * Instantiate object
     */
    public function __construct($auth_string = '')
    {
        // Check if lib cURL is enabled
        if (!function_exists('curl_init')) {
            throw new Exception('Lib cURL must be enabled on the server');
        }

        // Set auth string property
        $this->auth_string = $auth_string;

        // Instantiate cURL object
        $this->authenticate();
    }

    /**
     * Shutdown function.
     *
     * Closes the current cURL connection
     */
    public function shutdown()
    {
        if (!empty($this->ch)) {
            curl_close($this->ch);
        }
    }

    /**
     * authenticate function.
     *
     * Create a cURL instance with authentication headers
     */
    protected function authenticate()
    {
        $this->ch = curl_init();

        /**$headers = array(
            'Accept-Version: v10',
            'Accept: application/json',
        );**/

        /**umesh**/
        $headers = array(
            'X-Api-Key:117c6389a80980bd332afdc7a540ce03',
            'X-Auth-Token:f4a347718a0e68cfeb2fffe4f8f64d0e',
        );

        if (!empty($this->auth_string)) {
            $headers[] = 'Authorization: Basic '.base64_encode($this->auth_string);
        }

        $options = array(
            /**umesh**/ CURLOPT_URL, 'https://www.instamojo.com/api/1.1/payment-requests/', /**umesh **/
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_SSL_VERIFYPEER => true,
            CURLOPT_HTTPAUTH => CURLAUTH_BASIC,
            CURLOPT_HTTPHEADER => $headers,
        );

        curl_setopt_array($this->ch, $options);
    }
}
