<?php

namespace Instamojo\API;

/**
 * @class       Instamojo_Exception
 * @extends     Exception
 *
 * @since       0.1.0
 *
 * @category    Class
 *
 * @author      Patrick Tolvstein, Perfect Solution ApS
 * @docs        http://instamojo.com/api/
 */
class Exception extends \Exception
{
    /**
     * __Construct function.
     *
     * Redefine the exception so message isn't optional
     */
    public function __construct($message, $code = 0, self $previous = null)
    {
        // Make sure everything is assigned properly
        parent::__construct($message, $code, $previous);
    }
}
