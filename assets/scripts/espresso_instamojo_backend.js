( function( $ ) {
	"use strict";

	Instamojo.prototype.init = function() {
		// Add event handlers
		this.actionBox.on( 'click', '[data-action]', $.proxy( this.callAction, this ) );
	};

	Instamojo.prototype.callAction = function( e ) {
		e.preventDefault();
		var target = $( e.target );
		var action = target.attr( 'data-action' );

		if( typeof this[action] !== 'undefined' ) {
			var message = target.attr('data-confirm') || 'Are you sure you want to continue?';
			if( confirm( message ) ) {
				this[action]();
			}
		}
	};

	Instamojo.prototype.capture = function() {
		var request = this.request( {
			instamojo_action : 'capture'
		} );
	};

	Instamojo.prototype.captureAmount = function () {
		var request = this.request({
			instamojo_action: 'capture',
			instamojo_amount: $('#qp-balance__amount-field').val()
		} );
	};

	Instamojo.prototype.cancel = function() {
		var request = this.request( {
			instamojo_action : 'cancel'
		} );
	};

	Instamojo.prototype.refund = function() {
		var request = this.request( {
			instamojo_action : 'refund'
		} );
	};

	Instamojo.prototype.split_capture = function() {
		var request = this.request( {
			instamojo_action : 'splitcapture',
			amount : parseFloat( $('#instamojo_split_amount').val() ),
			finalize : 0
		} );
	};

	Instamojo.prototype.split_finalize = function() {
		var request = this.request( {
			instamojo_action : 'splitcapture',
			amount : parseFloat( $('#instamojo_split_amount').val() ),
			finalize : 1
		} );
	};

	Instamojo.prototype.request = function( dataObject ) {
		var that = this;
		var request = $.ajax( {
			type : 'POST',
			url : ajaxurl,
			dataType: 'json',
			data : $.extend( {}, { action : 'instamojo_manual_transaction_actions', txn_id : this.TXN_ID.val() }, dataObject ),
			beforeSend : $.proxy( this.showLoader, this, true ),
			success : function() {
				$.get( window.location.href, function( data ) {
					var newData = $(data).find( '#' + that.actionBox.attr( 'id' ) + ' .inside' ).html();
					that.actionBox.find( '.inside' ).html( newData );
					that.showLoader( false );
				} );
			}
		} );

		return request;
	};

	Instamojo.prototype.showLoader = function( e, show ) {
		if( show ) {
			this.actionBox.append( this.loaderBox );
		} else {
			this.actionBox.find( this.loaderBox ).remove();
		}
	};


	// DOM ready
	$(function() {
		new Instamojo().init();
	});

	function Instamojo() {
		this.actionBox 	= $( '#instamojo-payment-actions' );
		this.TXN_ID		= $( '#txn-admin-payment-txn-id-inp' );
		this.loaderBox 	= $( '<div class="loader"></div>');
	}

})(jQuery);