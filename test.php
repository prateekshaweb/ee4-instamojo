<?php

include_once "instamojo-php-master/src/Instamojo.php";
//include_once "instamojo-php-master/composer.json";

$ch = curl_init();

curl_setopt($ch, CURLOPT_URL, 'https://www.instamojo.com/api/1.1/payment-requests/');
curl_setopt($ch, CURLOPT_HEADER, FALSE);
curl_setopt($ch, CURLOPT_RETURNTRANSFER, TRUE);
curl_setopt($ch, CURLOPT_FOLLOWLOCATION, TRUE);
curl_setopt($ch, CURLOPT_HTTPHEADER,
            array("X-Api-Key:117c6389a80980bd332afdc7a540ce03",
                  "X-Auth-Token:f4a347718a0e68cfeb2fffe4f8f64d0e"));
$payload = Array(
    'purpose' => 'Test 1',
    'amount' => '500',
    'phone' => '9999999999',
    'buyer_name' => 'umesh kadam',
    'redirect_url' => 'https://prateeksha.in/clients/instamojo/',
    'send_email' => true,
    'webhook' => 'https://prateeksha.in/clients/instamojo',
    'send_sms' => true,
    'email' => 'umesh@prateeksha.com',
    'allow_repeated_payments' => false
);
curl_setopt($ch, CURLOPT_POST, true);
curl_setopt($ch, CURLOPT_POSTFIELDS, http_build_query($payload));
$response = curl_exec($ch);
curl_close($ch); 

echo $response;

?>
