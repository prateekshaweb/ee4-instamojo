=== Event Espresso - Instamojo (EE 4.x+) ===
Contributors: PerfectSolution
Tags: gateway, event espresso, eventespresso, event-espresso, instamojo, Instamojo, payment, gateway, event espresso instamojo, eventespresso instamojo, event-espresso instamojo
Requires at least: 1.0.0
Tested up to: 4.7.1
Stable tag: trunk
License: GPLv2
License URI: http://www.gnu.org/licenses/gpl-2.0.html

Integrates your Instamojo payment gateway into your Event Espresso 4 installation.

== Description ==
With Event Espresso - Instamojo, you are able to integrate your Instamojo gateway to your Event Espresso install. With a wide list of API features including secure capturing, refunding and cancelling payments directly from your Event Espresso transaction overview.

Make sure you have Event Espresso 4.x+ installed before using this plugin.

== Installation ==
1. Upload the 'event-espresso-instamojo' folder to /wp-content/plugins/ on your server.
2. Log in to WordPress administration, click on the 'Plugins' tab.
3. Find 'Event Espresso - Instamojo (EE 4.x+)' in the plugin overview and activate it.
4. Go to Event Espresso -> Payment Methods -> Instamojo.
5. Type in your settings to get started. Minimum requirements are the "Private key" and the "API key". You can get more information about this in the settings helper tab.
6. You are good to go.

== Changelog ==
= 1.0.0 =
* Initial plugin state